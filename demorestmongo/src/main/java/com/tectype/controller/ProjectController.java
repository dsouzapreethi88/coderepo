package com.tectype.controller;

import com.tectype.domain.Project;
import com.tectype.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectController {
    @Autowired
    ProjectService projectService;


    @RequestMapping("/project")
    public String home(Model model) {
        model.addAttribute("projects", projectService.findAllProjects());
        return "project";
    }

    @RequestMapping(value = "/addProject", method = RequestMethod.POST)
    public String addProject(@ModelAttribute Project project) {
       projectService.addProject(project);
        return "redirect:project";
    }

    /*@RequestMapping(value = "/deleteProject", method = RequestMethod.POST)
    public String deleteProject(@ModelAttribute String projectName) {
        projectService.findProjectById()
        projectService.deleteProject(project);
        return "redirect:project";
    }*/

    /*@RequestMapping(value = "/searchProject")
    public String search(Model model,@RequestParam String search) {
        *//*model.addAttribute("projects", projectService.searchProjects(search));
        model.addAttribute("search", search);*//*
        return "project";
    }*/




}
