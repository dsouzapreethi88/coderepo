package com.tectype.service;

import com.tectype.domain.Employee;
import com.tectype.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    public List<Employee> findAllEmployees(){
       return employeeRepository.findAll();
    }

    public Optional<Employee> findEmployeeById(int  employeeId){
        return employeeRepository.findById(employeeId);
    }

    public Employee addEmployee(Employee employee){
       return employeeRepository.save(employee);
    }

    public List<Employee> addEmployees(List<Employee> employees){
        return employeeRepository.saveAll(employees);
    }

    public boolean deleteEmployeeById(Integer employeeId){
        employeeRepository.deleteById(employeeId);
        return true;
    }

    public boolean deleteAllEmployees(){
        employeeRepository.deleteAll();
        return true;
    }

    public boolean deleteSelectedEmployees(List<Employee> employees){
        employeeRepository.deleteAll(employees);
        return true;
    }

    public boolean deleteEmployee(Employee employee){
        employeeRepository.delete(employee);
        return true;
    }

    public Employee updateEmployee(Employee employee){
        return employeeRepository.save(employee);
    }
}
