package com.tectype.service;

import com.tectype.domain.EmployeeProject;
import com.tectype.repository.EmployeeProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeProjectService {
    @Autowired
    EmployeeProjectRepository employeeProjectRepository;

    public List<EmployeeProject> findAllEmployeeProjects() {
        return employeeProjectRepository.findAll();
    }

    public Optional<EmployeeProject> findEmployeeProjectById(int id) {
        return employeeProjectRepository.findById(id);
    }

    public EmployeeProject addEmployeeProject(EmployeeProject employeeProject) {
        return employeeProjectRepository.save(employeeProject);
    }

    public List<EmployeeProject> addEmployeeProjects(List<EmployeeProject> employeeProjects) {
        return employeeProjectRepository.saveAll(employeeProjects);
    }

    public EmployeeProject updateEmployeeProject(EmployeeProject employeeProject) {
        return employeeProjectRepository.save(employeeProject);
    }

    public boolean deleteAllEmployeeEmployeeProjects() {
        employeeProjectRepository.deleteAll();
        return true;
    }
}
