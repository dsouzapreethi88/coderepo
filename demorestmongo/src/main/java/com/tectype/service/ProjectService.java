package com.tectype.service;

import com.tectype.domain.Project;
import com.tectype.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    ProjectRepository projectRepository;

    public List<Project> findAllProjects() {
        return projectRepository.findAll();
    }

    public Optional<Project> findProjectById(int projectId) {
        return projectRepository.findById(projectId);
    }

    /*public List<Project> findProjectByProjectDesc(String projectDescr) {
        return projectRepository.findByProjectDescription(projectDescr);
    }

    public List<Project> findProjectByProjectName(String projectName) {
        return projectRepository.findByProjectName(projectName);
    }*/

    public Project addProject(Project project) {
        int nextId=getMaxId()+1;
        project.setProjectId(nextId);
        return projectRepository.save(project);
    }


    public List<Project> addProjects(List<Project> projects) {
        return projectRepository.saveAll(projects);
    }

    public Project updateProject(Project project) {
        return projectRepository.save(project);

    }

    public boolean deleteAllProjects() {
        projectRepository.deleteAll();
        return true;
    }

    public boolean deleteProjectById(Integer projectId) {
        projectRepository.deleteById(projectId);
        return true;
    }

   /* public boolean deleteProjectByProjectName(String projectName) {
        projectRepository.deleteAll(findProjectByProjectName(projectName));
        return true;
    }*/

    /*public boolean deleteProjectByProjectDesc(String projectDesc) {
        projectRepository.deleteAll(findProjectByProjectDesc(projectDesc));
        return true;
    }*/

  /*  public Collection searchProjects(String search) {
        return projectRepository.searchProjects(search);
    }
*/
   /* public void searchProjects(String text) {
        Query query=new Query();
        *//*Query query=new Query();
        query.add*//*
       *//* query.addCriteria(Criteria.where("age").lt(50).gt(20));
        List<User> users = mongoTemplate.find(query,User.class);
        return mongoTemplate.find(Query.query(new Criteria()
                .orOperator(Criteria.where("description").regex(text, "i"),
                        Criteria.where("make").regex(text, "i"),
                        Criteria.where("model").regex(text, "i"))
        ), Project.class);*//*
    }*/



    public int getMaxId() {
        return projectRepository.getMaxId();
        /*Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "projectId"));
        query.limit(1);
        Project maxObject = mongoTemplate.findOne(query, Project.class);
        if(maxObject!=null){
            return maxObject.getProjectId();
        }
        return 0;*/
    }
}
