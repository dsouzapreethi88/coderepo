package com.tectype.repository;

import com.tectype.domain.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;

import java.util.List;

public class ProjectRepositoryImpl implements ProjectRepositoryCustom {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public int getMaxId() {
        Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "projectId"));
        query.limit(1);
        Project maxObject = mongoTemplate.findOne(query, Project.class);
        if(maxObject!=null){
            return maxObject.getProjectId();
        }
        return 0;
    }

 /*   @Query("SELECT p FROM Project p WHERE LOWER(p.projectName) = LOWER(:projectName)")
    public List<Project> findByProjectName(@Param("projectName") String projectName);*/
}
