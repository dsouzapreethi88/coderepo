package com.tectype.repository;

import com.tectype.domain.EmployeeProject;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeProjectRepository extends MongoRepository<EmployeeProject,Integer>{

}
