package com.tectype.repository;

import com.tectype.domain.Project;

import java.util.List;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.query.Param;

public interface ProjectRepositoryCustom {
    public int getMaxId();

}
