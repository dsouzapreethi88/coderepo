package com.tectype.repository;

import com.tectype.domain.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProjectRepository extends MongoRepository<Project,Integer>,ProjectRepositoryCustom{
}

