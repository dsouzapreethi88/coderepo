package com.tectype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemorestmongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemorestmongoApplication.class, args);
	}
}
